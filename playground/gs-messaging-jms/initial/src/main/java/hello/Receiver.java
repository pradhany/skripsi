package hello;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by Pradhany on 9/14/2016.
 */

@Component
public class Receiver {

    @JmsListener(destination = "demo.queue.spring", containerFactory = "myFactory")
    public void receiveMessage(Email email) {
        System.out.println("Received <" + email + ">");
    }


}