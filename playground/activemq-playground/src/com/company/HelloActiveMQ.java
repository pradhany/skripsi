package com.company;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by Pradhany on 9/14/2016.
 */
public class HelloActiveMQ {

    public static void main(String[] args) throws  Exception {
//        HelloWorldProducer producer = new HelloWorldProducer();
        HelloWorldConsumer consumer = new HelloWorldConsumer();

//        Thread threadProducer = new Thread(producer);
//        threadProducer.start();

        Thread threadConsumer = new Thread(consumer);
        threadConsumer.start();
    }

    public static class HelloWorldProducer implements Runnable {

        @Override
        public void run() {
            try {
                // Create a ConnectionFactory
                ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://Nusantara:61616");

                // Create a Connection
                Connection connection = connectionFactory.createConnection();
                connection.start();

                // Create a Session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                // create destination
                Destination destination = session.createQueue("demo.queue");

                // Create a MessageProducer from the Session to the Topic or
                // Queue
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

                // Create a messages
                String text = "Hello world! from "+Thread.currentThread().getName()+" : "+this.hashCode();
                TextMessage message = session.createTextMessage(text);

                // Tell procedure to send message
                System.out.println("Sent Message : "+message.hashCode()+ " : "+Thread.currentThread().getName());
                producer.send(message);

                // Clean Up
                session.close();
                connection.close();

            } catch (Exception e) {
                System.out.println("Caught : "+e);
                e.printStackTrace();

            }
        }
    }

    public static class HelloWorldConsumer implements Runnable, ExceptionListener {

        @Override
        public void run() {

            try {
                // Crete Connection Factory
                ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://Nusantara:61616");

                // create a connection
                Connection connection = connectionFactory.createConnection();
                connection.start();
                connection.setExceptionListener(this);

                // create a session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                // create destination
                Destination destination = session.createQueue("demo.queue");

                // create message consumer from session to the topic or queue
                MessageConsumer consumer = session.createConsumer(destination);

                // Wait for message
                Message message = consumer.receive();

                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    System.out.println("Received : "+text);
                } else {
                    System.out.println("Received : "+message);
                }

                consumer.close();
                session.close();
                connection.close();

            } catch (Exception e) {
                System.out.println("Caught : "+e);
                e.printStackTrace();
            }

        }

        @Override
        public void onException(JMSException e) {
            System.out.println("JMS Exception occured.  Shutting down client.");
        }
    }
}
