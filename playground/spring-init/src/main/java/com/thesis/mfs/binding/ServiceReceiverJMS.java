package com.thesis.mfs.binding;

import com.thesis.mfs.model.BalanceManagerRq;
import com.thesis.mfs.operation.opAdjustBalance.AdjustBalanceOperation;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Created by Pradhany on 9/16/2016.
 */

@Component
public class ServiceReceiverJMS {

    @JmsListener(destination = "core.service.balanceManager", containerFactory = "myFactory")
    public void receiveRequest(BalanceManagerRq balanceManagerRq) {

        String operationName = balanceManagerRq.getOperationName();
        switch (operationName) {
            case "opAdjustBalance" : {
                selectOpAdjustBalance(balanceManagerRq);
                break;
            }
        }


    }

    public void selectOpAdjustBalance(BalanceManagerRq balanceManagerRq) {
        AdjustBalanceOperation adjustBalanceOperation = new AdjustBalanceOperation(balanceManagerRq);
        adjustBalanceOperation.executeAdjustBalance();
    }
}
