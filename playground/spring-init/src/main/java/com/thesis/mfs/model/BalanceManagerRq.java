package com.thesis.mfs.model;

import java.io.Serializable;

/**
 * Created by Pradhany on 9/16/2016.
 */
public class BalanceManagerRq implements Serializable{

    private String operationName;

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
}
